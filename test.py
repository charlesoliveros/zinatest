"""
	Bouncy Numbers Problem
	Author: Carlos Oliveros
	Email: ing.oliveros87@gmail.com
	codified with python3.4 
"""

class MyNumber(object):

	def __init__(self, num = 0):
		self.num = num

	def is_increasing(self):
		digits = self.get_digits()
		temp = None
		for d in digits:
			if temp:
				if not d >= temp:
					return False
			temp = d
		return True

	def is_decreasing(self):
		digits = self.get_digits()
		temp = None
		for d in digits:
			if temp:
				if not d <= temp:
					return False
			temp = d
		return True

	def is_bouncy(self):
		if not self.is_increasing():
			if not self.is_decreasing():
				return True
		return False

	def get_digits(self):
		return [d for d in str(self.num)]


class MyProgram(object):

	
	# percen_limit = proporcion minima de bouncy Numbers
	def __init__(self, percen_limit=0.50):
		self.percen_limit = percen_limit

	# encentra el numero con la proporcion minima de 
	# Bouncy numbers
	def run(self):
		number = 100
		cont_bouncys = 0
		porcen_bouncys = 0
		continue_run = True
		while continue_run:
			mynumber = MyNumber(number)
			if(mynumber.is_bouncy()):
				cont_bouncys += 1
			porcen_bouncys =  cont_bouncys / number
			if porcen_bouncys < self.percen_limit:
				number += 1
			else:
				continue_run = False
		self.print_results(number,cont_bouncys,porcen_bouncys)

	def print_results(self, last_number, cont_bouncys, 
	 	porcen_bouncys):
		print('----'*10)
		print('LEAST NUMBER:', last_number)
		print('Percen Limit: ', self.percen_limit)
		#print('Cont bouncys: ', cont_bouncys)
		#print('Porcen bouncys', porcen_bouncys)
		print('----'*10)

myprogram = MyProgram(0.99)
myprogram.run()